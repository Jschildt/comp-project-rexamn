# Computer Engineering Semester Project I [Re-examn]
This project extracts data from 4 Ultrasonic Range Sensors connected to an Arduino Pro Micro, and through the use of ROS, uses it to modify a obstacle avoidance module running on a virtual machine simulation, using turtlebot and RVIZ. 


## Getting Started

### Dependencies & Nessecary Hardware

* Arduino Pro Micro, 4 URF's. 
* Arduino Ide
* Ros Melodic on Ubuntu 18.04
* Any relevant programs or packages provided to us in the tutorials from week 9

### Hardware Setup

* Connect the AN pin of your 4 URF sensors, to the following analog ports, aswell as GND to GND and +5V to VCC:
    * Back-sensor to A0
    * Left sensor to A1
  * Front sensor to A2
  * Right sensor to A3
  

* Connect your USB cable from your computer to the Arduino Pro Micro.


### Software Setup
* Open terminal on VM
```
cd catkin_ws/src/turtlebot3
```
```
git clone git@gitlab.com:Jschildt/comp-project-rexamn.git
```
```
cd /Comp-Project-Rexamn
```
```
chmod -x OAM.py 
```
Open ArduinoOAM.ino, when prompted about creating a sketch folder, press accept. 

In the Arduino Ide, go to Tools - Manage Libraries, and search for "Rosserial Arduino Library". Install this, then close the library manager, and restart your IDE. 
Then upload the ArduinoOAM.ino to your Arduino Micro.

In your VM terminal, do:
```
cd ~/catkin_ws
```
```
catkin_make
```
Launch the ros master:
```
roslaunch turtlebot3_gazebo turtlebot3_world.launch 
```
Open a new terminal and do:
##### (This allows the communication between Arduino and your ROS subscribers)
```
rosrun rosserial_python serial_node.py /dev/ttyACM0
```
And in a third terminal, navigate to your directory including OAM.py
```
cd catkin_ws/src/turtlebot3/Comp-Project-Rexamn
```
And launch it
```
python OAM.py
```

If you wish to create a mapping of what the turtlebot explores using rviz, then in a new terminal do:
```
roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping
```
## Help

It is STRONGLY recommended to have the Arduino Ide on your actual OS, and not on your VM, as there is a high chance you will have big difficulties uploading the .ino files to your Arduino Pro Micro. 

Also do note that when utilizing the gmapping, the turtlebot simulation will most likely lag (intensity based mostly upon RAM avaliable).

##### If you get a similar error to following:
"Error opening serial: [Errno 2] could not open port 
/dev/ttyACM0: [Errno 2] No such file or directory: '/dev/ttyACM0"

Then you need to goto Devices<USB and select the arduino board. 
(it should be named something along the way of "Arduino LLC Arduino Micro [XXXX]")

## Authors

* Henrik Buhl - 201905590@post.au.dk
* Jeppe S. Schildt -201905520@post.au.dk
* Akif Balikci - 201906376@post.au.dk



