#define USE_USBCON		//This is essential for when we have to publish in turtlebot
#include <ros.h>		//includes ros related libraries
#include <ros/time.h>		
#include <sensor_msgs/Range.h>	//Datatype we use to publish data, same datatype is used to subscribe through python.

ros::NodeHandle  nh;            //nodehandle an object which represents the ROS node

//declaring instances for range messages
sensor_msgs::Range range_back;   
sensor_msgs::Range range_left;
sensor_msgs::Range range_center;
sensor_msgs::Range range_right;

//Creates publisher objects for the sensors
ros::Publisher pub_range_back("/ultrasound_back", &range_back);    
ros::Publisher pub_range_left("/ultrasound_left", &range_left);
ros::Publisher pub_range_center("/ultrasound_center", &range_center);
ros::Publisher pub_range_right("/ultrasound_right", &range_right);

//The next part is used to set the parameters for the 4 sensors in their topics
void sensor_msg_init(sensor_msgs::Range &range_name, char *frame_id_name)
{
  range_name.radiation_type = sensor_msgs::Range::ULTRASOUND;
  range_name.header.frame_id = frame_id_name;
  range_name.field_of_view = 0.26;
  range_name.min_range = 0.0;
  range_name.max_range = 2.0;
}

void setup() {
  nh.initNode();
  nh.advertise(pub_range_back);		//initializes the publishers
  nh.advertise(pub_range_left);
  nh.advertise(pub_range_right);
  nh.advertise(pub_range_center);
  sensor_msg_init(range_left, "/ultrasound_left");	
  sensor_msg_init(range_center, "/ultrasound_center");
  sensor_msg_init(range_right, "/ultrasound_right");
  sensor_msg_init(range_back, "/ultrasound_back");
  
  pinMode(A0,INPUT);	//set the pins
  pinMode(A1,INPUT);
  pinMode(A2,INPUT);
  pinMode(A3,INPUT);
  Serial.begin(57600);		// Serial.begin allows us to use rostopic on our VM. 
  nh.getHardware()->setBaud(57600);
}

// the loop function runs over and over again forever
void loop() {
  range_back.range = analogRead(A0) * 1.5;	//sets the range on the instances to be what is on the pins
  range_left.range = analogRead(A1) * 1.5;	//we multiply by 1.5 because the data that it was putting out 
  range_center.range = analogRead(A2) * 1.5;	//made sense with the sensors having a min range at 6 inches or 15cm
  range_right.range = analogRead(A3) * 1.5;
  delay(100);
  pub_range_back.publish(&range_back);		//publish data
  pub_range_left.publish(&range_left);
  pub_range_center.publish(&range_center);
  pub_range_right.publish(&range_right);
  nh.spinOnce();				//loops it
}