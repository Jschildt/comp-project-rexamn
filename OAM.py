#!/usr/bin/env python
import rospy                             	
from geometry_msgs.msg import Twist             #Twist is used to tell the turtlebot what it's angular and kinetic velocity's should be
from sensor_msgs.msg import LaserScan           #we use this to determine where walls are by using msg.ranges[region/area]
from sensor_msgs.msg import Range               #Datatype used for getting the values from arduino.

def callback_regions(msg):
    regions = {         
                                                #we use function min to find the scan which is nearest to an object
        'front right': min(msg.ranges[-54:-19]),
        'front':  min([msg.ranges[-18:-1], msg.ranges[0:18]]),  #we cannot take from area -18 to 18 so then we will take min of area (-18 to -1 vs 0 to 18) 
        'front left':  min(msg.ranges[19:54]),
                                                #we do not use left or right because of the ultrasonic sensors 
    }
    callback(regions)

def callback(regions):                  
    command.angular.z = 0.0     #sets angular velocity to 0
    command.linear.x = 0.0      #sets velocity to 0
    d = 0.4			#This is a variable which determines whether the turtlebot is close to something or not

    if(regions['front'] > d and regions['front left'] > d and regions['front right'] > d):   #case 1
        print('case 1')			#we print the case numbers so that we can better see what is going on
        command.angular.z = 0.0     	#do not rotate
        command.linear.x = 0.3       	#drive
    elif(regions['front'] < d and regions['front left'] > d and regions['front right'] > d): #case 2 
        print('case 2')
        command.linear.x = 0.0       	#stop
        command.angular.z = 0.6     	#rotate to the left
    elif(regions['front'] > d and regions['front left'] > d and regions['front right'] < d): #case 3
        print('case 3')
        command.linear.x = 0.0       	#stop
        command.angular.z = 0.6     	#rotate to the left
    elif(regions['front'] > d and regions['front left'] < d and regions['front right'] > d): #case 4
        print('case 4')
        command.linear.x = 0.0       	#stop
        command.angular.z = -0.6     	#rotate to the right
    elif(regions['front'] < d and regions['front left'] > d and regions['front right'] < d): #case 5
        print('case 5')
        command.linear.x = 0.0       	#stop
        command.angular.z = 0.6     	#rotate left
    elif(regions['front'] < d and regions['front left'] < d and regions['front right'] > d): #case 6
        print('case 6')
        command.linear.x = 0.0       	#stop
        command.angular.z = -0.2     	#rotate right
    elif(regions['front'] < d and regions['front left'] < d and regions['front right'] < d): #case 7
        print('case 7')
        command.linear.x =  -0.3  	    #drive backwards
        command.angular.z = 0.6     	#rotate left
    elif(regions['front'] > d and regions['front left'] < d and regions['front right'] < d): #case 8
        print('case 8')
        command.linear.x = -0.3       	#drive backwards
        command.angular.z = 0.6     	#rotate left
    pub.publish(command)            	#publishes what is in command

def ultracenter(range): #sensor in front
    if(range.range <= 30):
        command.linear.x = -0.3     #goes backwards
        command.angular.z = 0.0        
        #print(range)               #comment out the if statement and use print to see how good the sensors are and calibrate them 
        pub.publish(command)            	#publishes what is in command
    
def ultraleft(range):   #sensor on the left
    if(range.range <= 30):
        command.angular.z = -0.6    #turn to the right 
        command.linear.x = 0.0   
        #print(range)
        pub.publish(command)            	#publishes what is in command

def ultraright(range):  #sensor on the right
    if(range.range <= 30):         
        command.angular.z = 0.6     #turns to the left
        command.linear.x = 0.0
        #print(range)
        pub.publish(command)            	#publishes what is in command

def ultraback(range):   #sensor on back
    if(range.range <= 30):             
        command.linear.x = 0.3      #goes forward
        command.angular.z = 0.0
        #print(range)
        pub.publish(command)            	#publishes what is in command

rospy.init_node('rotate_robot', anonymous=True) 		#initiate node "rotate_robot" 
subl = rospy.Subscriber('scan', LaserScan, callback_regions)    #subscribe to Laserscan

activesensor = True    #I don't always want my more complicated and stupidified teleop on

if(activesensor == True):        #Subscribers for all 4 sensors
    subuc = rospy.Subscriber('/ultrasound_center', Range, ultracenter)
    subul = rospy.Subscriber('/ultrasound_left', Range, ultraleft)
    subub = rospy.Subscriber('/ultrasound_back', Range, ultraback)
    subur = rospy.Subscriber('/ultrasound_right', Range, ultraright)

pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)  	#publish to twist
command = Twist()						#command makes it more official that this is where we want it to do something
r = rospy.Rate(125)  						#sets the object rate to 125Hz
rospy.spin()